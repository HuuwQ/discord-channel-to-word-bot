# Bot discord

## Crée un word à partir d'un channel grâce à la commande !word

[![pipeline status](https://gitlab.com/HuuwQ/discord-channel-to-word-bot/badges/main/pipeline.svg)](https://gitlab.com/HuuwQ/discord-channel-to-word-bot/-/commits/main)
[![Latest Release](https://gitlab.com/HuuwQ/discord-channel-to-word-bot/-/badges/release.svg)](https://gitlab.com/HuuwQ/discord-channel-to-word-bot/-/releases)

- [Bot discord](#bot-discord)
  - [Pré-requis](#pr--requis)
    - [Installation](#installation)
    - [Installer les dépendances](#installer-les-d-pendances)
  - [Lancer le projet](#lancer-le-projet)

## Pré-requis

### Installation

- Installer nodejs
- Installer npm

### Installer les dépendances

Pour lancer le projet, il faut installer les dépendances :

- npm i

## Lancer le projet

Il suffit de lancer la commande suivante avec l'environnement de chargé :

> npm start
